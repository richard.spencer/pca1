// Export pages
export 'splash_screen/splash_screen_widget.dart' show SplashScreenWidget;
export 'login/login_widget.dart' show LoginWidget;
export 'my_tasks_o_l_d/my_tasks_o_l_d_widget.dart' show MyTasksOLDWidget;
export 'register/register_widget.dart' show RegisterWidget;
export 'completed_tasks/completed_tasks_widget.dart' show CompletedTasksWidget;
export 'edit_profile/edit_profile_widget.dart' show EditProfileWidget;
export 'task_details/task_details_widget.dart' show TaskDetailsWidget;
export 'my_profile/my_profile_widget.dart' show MyProfileWidget;
export 'change_password/change_password_widget.dart' show ChangePasswordWidget;
export 'edit_company/edit_company_widget.dart' show EditCompanyWidget;
export 'my_companies/my_companies_widget.dart' show MyCompaniesWidget;
export 'edit_project/edit_project_widget.dart' show EditProjectWidget;
export 'my_projects/my_projects_widget.dart' show MyProjectsWidget;
export 'my_instalments/my_instalments_widget.dart' show MyInstalmentsWidget;
export 'my_tasks/my_tasks_widget.dart' show MyTasksWidget;
