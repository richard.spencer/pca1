// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '../actions/index.dart'; // Imports custom actions
import '../../flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';

// Begin custom widget code
class InstalmentTable extends StatefulWidget {
  const InstalmentTable({
    Key? key,
    this.width,
    this.height,
    this.amountInstalments,
    this.descriptionInstalments,
  }) : super(key: key);

  final double? width;
  final double? height;
  final List<double?>? amountInstalments;
  final List<String?>? descriptionInstalments;

  @override
  _InstalmentTableState createState() => _InstalmentTableState();
}

class _InstalmentTableState extends State<InstalmentTable> {
  @override
  Widget build(BuildContext context) {
    int noOfInstalments = widget.amountInstalments!.length;
    List<Widget> rowWidgets = [];
    List<TextEditingController> amountTextControllers = [];
    List<TextEditingController> descriptionTextControllers = [];
    for (int i = 0; i < noOfInstalments; i++) {
      amountTextControllers.add(
          TextEditingController(text: widget.amountInstalments![i].toString()));
      descriptionTextControllers
          .add(TextEditingController(text: widget.descriptionInstalments![i]));
      rowWidgets.add((Row(children: [
        Text('(A6)' + i.toString(), style: TextStyle(color: Colors.pink)),
        Text('(A7)' + widget.amountInstalments![i].toString(),
            style: TextStyle(color: Colors.amber)),
        TextFormField(
          controller: amountTextControllers[i],
          autofocus: true,
          obscureText: false,
          decoration: InputDecoration(
            labelText: 'Instalment ' + i.toString(),
            hintText: '',
            hintStyle: FlutterFlowTheme.of(context).bodyText2,
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(4.0),
                topRight: Radius.circular(4.0),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(4.0),
                topRight: Radius.circular(4.0),
              ),
            ),
          ),
          style: FlutterFlowTheme.of(context).bodyText1,
        ),
        TextFormField(
          controller: descriptionTextControllers[i],
          autofocus: true,
          obscureText: false,
          decoration: InputDecoration(
            labelText: 'Description ' + i.toString(),
            hintText: '',
            hintStyle: FlutterFlowTheme.of(context).bodyText2,
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(4.0),
                topRight: Radius.circular(4.0),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0x00000000),
                width: 1,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(4.0),
                topRight: Radius.circular(4.0),
              ),
            ),
          ),
          style: FlutterFlowTheme.of(context).bodyText1,
        ),
      ])));
    }

    return Column(children: [
      Container(
          color: Colors.cyan, child: Text('<A1>' + noOfInstalments.toString())),
      Container(
          color: Colors.yellow,
          child: Text('<A2>' + widget.amountInstalments![0].toString())),
      Container(
          color: Colors.yellow,
          child: Text('<A3>' + rowWidgets.length.toString())),
      ...rowWidgets,
      //ListView(children: rowWidgets),
    ]);
  }
}
