// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '../actions/index.dart'; // Imports custom actions
import '../../flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';
// Begin custom widget code
// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '../actions/index.dart'; // Imports custom actions
import '../../flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';

// Begin custom widget code
class TextWithUpdateOrder extends StatefulWidget {
  const TextWithUpdateOrder({
    Key? key,
    this.width,
    this.height,
    this.order,
  }) : super(key: key);

  final double? width;
  final double? height;
  final int? order;

  @override
  _TextWithUpdateOrderState createState() => _TextWithUpdateOrderState();
}

class _TextWithUpdateOrderState extends State<TextWithUpdateOrder> {
  @override
  Widget build(BuildContext context) {
    print('(A5)${FFAppState().highestOrder}&${widget.order!}');

    if (widget.order! >= FFAppState().highestOrder) {
      FFAppState().highestOrder = widget.order!;
    }
    if ((FFAppState().highestOrder == null) ||
        (FFAppState().highestOrder == 0)) {
      FFAppState().highestOrder = 2;
    }
    print('(A7)${FFAppState().highestOrder}&${widget.order!}');
    if ((widget.order == null) || (widget.order == 0)) {
      FFAppState().highestOrder = 1;
      return Text(
        '1',
        style: FlutterFlowTheme.of(context).bodyText2,
      );
    }
    return Text(
      widget.order.toString(),
      style: FlutterFlowTheme.of(context).bodyText2,
    );
  }
}
