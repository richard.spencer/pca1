// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '../actions/index.dart'; // Imports custom actions
import '../../flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';
// Begin custom widget code
// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import 'package:flutter/material.dart';

class CompanyDropdown extends StatefulWidget {
  const CompanyDropdown({
    Key? key,
    this.width,
    this.height,
    this.company,
  }) : super(key: key);

  final double? width;
  final double? height;
  final DocumentReference? company;

  @override
  _CompanyDropdownState createState() => _CompanyDropdownState();
}

class _CompanyDropdownState extends State<CompanyDropdown> {
  @override
  Widget build(BuildContext context) {
    DocumentReference<Object?>? chosenCompany = FFAppState().companyChosen;

    Widget companyQueryDropdown = StreamBuilder<List<CompaniesRecord>>(
        stream: queryCompaniesRecord(
          limit: 100,
        ),
        builder: (context, snapshot) {
          // Customize what your widget looks like when it's loading.
          if (!snapshot.hasData) {
            return Center(
              child: SizedBox(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(
                  color: FlutterFlowTheme.of(context).primaryColor,
                ),
              ),
            );
          }
          List<CompaniesRecord> companiesRecordList = snapshot.data!;
          int noOfCompanies = companiesRecordList.length;
          //List<String> displayNames = [];
          List<DropdownMenuItem<DocumentReference<Object?>?>>
              dropdownMenuItems = [];
          for (int i = 0; i < noOfCompanies; i++) {
            String displayName =
                companiesRecordList[i].companyName ?? i.toString();
            dropdownMenuItems.add(DropdownMenuItem(
                child: (Text(displayName)),
                value: companiesRecordList[i].reference));
          }
          return Column(children: [
            DropdownButton<DocumentReference<Object?>?>(
              value: chosenCompany,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: const TextStyle(color: Colors.green),
              underline: Container(
                height: 2,
                color: Colors.blue,
              ),
              onChanged: (newCompany) {
                setState(() {
                  chosenCompany = newCompany!;
                  FFAppState().companyChosen = newCompany;
                });
              },
              items: dropdownMenuItems,
            )
          ]);
        });

    return companyQueryDropdown;
  }
}
