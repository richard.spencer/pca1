// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import '../actions/index.dart'; // Imports custom actions
import '../../flutter_flow/custom_functions.dart'; // Imports custom functions
import 'package:flutter/material.dart';
// Begin custom widget code
// Automatic FlutterFlow imports
import '../../backend/backend.dart';
import '../../flutter_flow/flutter_flow_theme.dart';
import '../../flutter_flow/flutter_flow_util.dart';
import 'index.dart'; // Imports other custom widgets
import 'package:flutter/material.dart';

class UserDropdown extends StatefulWidget {
  const UserDropdown({
    Key? key,
    this.width,
    this.height,
    this.user,
    this.chosenUserIndex,
  }) : super(key: key);

  final double? width;
  final double? height;
  final DocumentReference? user;
  final int? chosenUserIndex;

  @override
  _UserDropdownState createState() => _UserDropdownState();
}

class _UserDropdownState extends State<UserDropdown> {
  @override
  void initState() {
    super.initState();
  }

  void setLocalStateUser(DocumentReference? newUser) {
    switch (widget.chosenUserIndex) {
      case 1:
        FFAppState().user1Chosen = newUser;
        break;
      case 2:
        FFAppState().user2Chosen = newUser;
        break;
      case 3:
        FFAppState().user3Chosen = newUser;
        break;
      default:
        FFAppState().user1Chosen = newUser;
        ;
    }
  }

  @override
  Widget build(BuildContext context) {
    DocumentReference<Object?>? chosenUser;
    switch (widget.chosenUserIndex) {
      case 1:
        chosenUser = FFAppState().user1Chosen;
        break;
      case 2:
        chosenUser = FFAppState().user2Chosen;
        break;
      case 3:
        chosenUser = FFAppState().user3Chosen;
        break;
      default:
        chosenUser = FFAppState().user1Chosen;
    }
    //chosenUser = widget.user;
    //setLocalStateUser(widget.user);
    Widget userQueryDropdown = StreamBuilder<List<UsersRecord>>(
        stream: queryUsersRecord(
          limit: 100,
        ),
        builder: (context, snapshot) {
          // Customize what your widget looks like when it's loading.
          if (!snapshot.hasData) {
            return Center(
              child: SizedBox(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(
                  color: FlutterFlowTheme.of(context).primaryColor,
                ),
              ),
            );
          }
          List<UsersRecord> usersRecordList = snapshot.data!;
          int noOfUsers = usersRecordList.length;
          List<String> displayNames = [];
          List<DropdownMenuItem<DocumentReference<Object?>?>>
              dropdownMenuItems = [];
          // String email = '(' + chosenUser.email.toString() + ')';
          for (int i = 0; i < noOfUsers; i++) {
            String displayName = usersRecordList[i].displayName ?? i.toString();
            dropdownMenuItems.add(DropdownMenuItem(
                child: (Text(displayName)),
                value: usersRecordList[i].reference));
          }

          //dropdownValue = displayNames[0] ?? 0.toString();
          return Column(children: [
            // Text(email),
            //Text((FFAppState().userChosen ?? ).email.toString()),
            DropdownButton<DocumentReference<Object?>?>(
              value: chosenUser,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: const TextStyle(color: Colors.green),
              underline: Container(
                height: 2,
                color: Colors.blue,
              ),
              onChanged: (newUser) {
                setState(() {
                  chosenUser = newUser!;
                  setLocalStateUser(chosenUser);
                });
              },
              items: dropdownMenuItems,
            )
          ]);
        });

    return userQueryDropdown;
  }
}
