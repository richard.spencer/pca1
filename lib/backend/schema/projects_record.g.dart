// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects_record.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectsRecord> _$projectsRecordSerializer =
    new _$ProjectsRecordSerializer();

class _$ProjectsRecordSerializer
    implements StructuredSerializer<ProjectsRecord> {
  @override
  final Iterable<Type> types = const [ProjectsRecord, _$ProjectsRecord];
  @override
  final String wireName = 'ProjectsRecord';

  @override
  Iterable<Object?> serialize(Serializers serializers, ProjectsRecord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.projectName;
    if (value != null) {
      result
        ..add('project_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.projectType;
    if (value != null) {
      result
        ..add('project_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.clientCompany;
    if (value != null) {
      result
        ..add('client_company')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    value = object.clientContact;
    if (value != null) {
      result
        ..add('client_contact')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    value = object.projectSequenceNumber;
    if (value != null) {
      result
        ..add('project_sequence_number')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.currentInstalmentIndex;
    if (value != null) {
      result
        ..add('current_instalment_index')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.statusCurrentInstalment;
    if (value != null) {
      result
        ..add('status_current_instalment')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.projectDescription;
    if (value != null) {
      result
        ..add('project_description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.tasks;
    if (value != null) {
      result
        ..add('tasks')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BuiltList, const [
              const FullType(
                  DocumentReference, const [const FullType.nullable(Object)])
            ])));
    }
    value = object.teamLeader;
    if (value != null) {
      result
        ..add('team_leader')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    value = object.notes;
    if (value != null) {
      result
        ..add('notes')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.files;
    if (value != null) {
      result
        ..add('files')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    value = object.feedback;
    if (value != null) {
      result
        ..add('feedback')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ffRef;
    if (value != null) {
      result
        ..add('Document__Reference__Field')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    return result;
  }

  @override
  ProjectsRecord deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectsRecordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'project_name':
          result.projectName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'project_type':
          result.projectType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'client_company':
          result.clientCompany = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
        case 'client_contact':
          result.clientContact = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
        case 'project_sequence_number':
          result.projectSequenceNumber = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'current_instalment_index':
          result.currentInstalmentIndex = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'status_current_instalment':
          result.statusCurrentInstalment = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'project_description':
          result.projectDescription = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'tasks':
          result.tasks.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(
                    DocumentReference, const [const FullType.nullable(Object)])
              ]))! as BuiltList<Object?>);
          break;
        case 'team_leader':
          result.teamLeader = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
        case 'notes':
          result.notes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'files':
          result.files.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(String)]))!
              as BuiltList<Object?>);
          break;
        case 'feedback':
          result.feedback = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'Document__Reference__Field':
          result.ffRef = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectsRecord extends ProjectsRecord {
  @override
  final String? projectName;
  @override
  final String? projectType;
  @override
  final String? status;
  @override
  final DocumentReference<Object?>? clientCompany;
  @override
  final DocumentReference<Object?>? clientContact;
  @override
  final int? projectSequenceNumber;
  @override
  final int? currentInstalmentIndex;
  @override
  final String? statusCurrentInstalment;
  @override
  final String? projectDescription;
  @override
  final BuiltList<DocumentReference<Object?>>? tasks;
  @override
  final DocumentReference<Object?>? teamLeader;
  @override
  final String? notes;
  @override
  final BuiltList<String>? files;
  @override
  final String? feedback;
  @override
  final DocumentReference<Object?>? ffRef;

  factory _$ProjectsRecord([void Function(ProjectsRecordBuilder)? updates]) =>
      (new ProjectsRecordBuilder()..update(updates))._build();

  _$ProjectsRecord._(
      {this.projectName,
      this.projectType,
      this.status,
      this.clientCompany,
      this.clientContact,
      this.projectSequenceNumber,
      this.currentInstalmentIndex,
      this.statusCurrentInstalment,
      this.projectDescription,
      this.tasks,
      this.teamLeader,
      this.notes,
      this.files,
      this.feedback,
      this.ffRef})
      : super._();

  @override
  ProjectsRecord rebuild(void Function(ProjectsRecordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectsRecordBuilder toBuilder() =>
      new ProjectsRecordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectsRecord &&
        projectName == other.projectName &&
        projectType == other.projectType &&
        status == other.status &&
        clientCompany == other.clientCompany &&
        clientContact == other.clientContact &&
        projectSequenceNumber == other.projectSequenceNumber &&
        currentInstalmentIndex == other.currentInstalmentIndex &&
        statusCurrentInstalment == other.statusCurrentInstalment &&
        projectDescription == other.projectDescription &&
        tasks == other.tasks &&
        teamLeader == other.teamLeader &&
        notes == other.notes &&
        files == other.files &&
        feedback == other.feedback &&
        ffRef == other.ffRef;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                0,
                                                                projectName
                                                                    .hashCode),
                                                            projectType
                                                                .hashCode),
                                                        status.hashCode),
                                                    clientCompany.hashCode),
                                                clientContact.hashCode),
                                            projectSequenceNumber.hashCode),
                                        currentInstalmentIndex.hashCode),
                                    statusCurrentInstalment.hashCode),
                                projectDescription.hashCode),
                            tasks.hashCode),
                        teamLeader.hashCode),
                    notes.hashCode),
                files.hashCode),
            feedback.hashCode),
        ffRef.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ProjectsRecord')
          ..add('projectName', projectName)
          ..add('projectType', projectType)
          ..add('status', status)
          ..add('clientCompany', clientCompany)
          ..add('clientContact', clientContact)
          ..add('projectSequenceNumber', projectSequenceNumber)
          ..add('currentInstalmentIndex', currentInstalmentIndex)
          ..add('statusCurrentInstalment', statusCurrentInstalment)
          ..add('projectDescription', projectDescription)
          ..add('tasks', tasks)
          ..add('teamLeader', teamLeader)
          ..add('notes', notes)
          ..add('files', files)
          ..add('feedback', feedback)
          ..add('ffRef', ffRef))
        .toString();
  }
}

class ProjectsRecordBuilder
    implements Builder<ProjectsRecord, ProjectsRecordBuilder> {
  _$ProjectsRecord? _$v;

  String? _projectName;
  String? get projectName => _$this._projectName;
  set projectName(String? projectName) => _$this._projectName = projectName;

  String? _projectType;
  String? get projectType => _$this._projectType;
  set projectType(String? projectType) => _$this._projectType = projectType;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  DocumentReference<Object?>? _clientCompany;
  DocumentReference<Object?>? get clientCompany => _$this._clientCompany;
  set clientCompany(DocumentReference<Object?>? clientCompany) =>
      _$this._clientCompany = clientCompany;

  DocumentReference<Object?>? _clientContact;
  DocumentReference<Object?>? get clientContact => _$this._clientContact;
  set clientContact(DocumentReference<Object?>? clientContact) =>
      _$this._clientContact = clientContact;

  int? _projectSequenceNumber;
  int? get projectSequenceNumber => _$this._projectSequenceNumber;
  set projectSequenceNumber(int? projectSequenceNumber) =>
      _$this._projectSequenceNumber = projectSequenceNumber;

  int? _currentInstalmentIndex;
  int? get currentInstalmentIndex => _$this._currentInstalmentIndex;
  set currentInstalmentIndex(int? currentInstalmentIndex) =>
      _$this._currentInstalmentIndex = currentInstalmentIndex;

  String? _statusCurrentInstalment;
  String? get statusCurrentInstalment => _$this._statusCurrentInstalment;
  set statusCurrentInstalment(String? statusCurrentInstalment) =>
      _$this._statusCurrentInstalment = statusCurrentInstalment;

  String? _projectDescription;
  String? get projectDescription => _$this._projectDescription;
  set projectDescription(String? projectDescription) =>
      _$this._projectDescription = projectDescription;

  ListBuilder<DocumentReference<Object?>>? _tasks;
  ListBuilder<DocumentReference<Object?>> get tasks =>
      _$this._tasks ??= new ListBuilder<DocumentReference<Object?>>();
  set tasks(ListBuilder<DocumentReference<Object?>>? tasks) =>
      _$this._tasks = tasks;

  DocumentReference<Object?>? _teamLeader;
  DocumentReference<Object?>? get teamLeader => _$this._teamLeader;
  set teamLeader(DocumentReference<Object?>? teamLeader) =>
      _$this._teamLeader = teamLeader;

  String? _notes;
  String? get notes => _$this._notes;
  set notes(String? notes) => _$this._notes = notes;

  ListBuilder<String>? _files;
  ListBuilder<String> get files => _$this._files ??= new ListBuilder<String>();
  set files(ListBuilder<String>? files) => _$this._files = files;

  String? _feedback;
  String? get feedback => _$this._feedback;
  set feedback(String? feedback) => _$this._feedback = feedback;

  DocumentReference<Object?>? _ffRef;
  DocumentReference<Object?>? get ffRef => _$this._ffRef;
  set ffRef(DocumentReference<Object?>? ffRef) => _$this._ffRef = ffRef;

  ProjectsRecordBuilder() {
    ProjectsRecord._initializeBuilder(this);
  }

  ProjectsRecordBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _projectName = $v.projectName;
      _projectType = $v.projectType;
      _status = $v.status;
      _clientCompany = $v.clientCompany;
      _clientContact = $v.clientContact;
      _projectSequenceNumber = $v.projectSequenceNumber;
      _currentInstalmentIndex = $v.currentInstalmentIndex;
      _statusCurrentInstalment = $v.statusCurrentInstalment;
      _projectDescription = $v.projectDescription;
      _tasks = $v.tasks?.toBuilder();
      _teamLeader = $v.teamLeader;
      _notes = $v.notes;
      _files = $v.files?.toBuilder();
      _feedback = $v.feedback;
      _ffRef = $v.ffRef;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectsRecord other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ProjectsRecord;
  }

  @override
  void update(void Function(ProjectsRecordBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ProjectsRecord build() => _build();

  _$ProjectsRecord _build() {
    _$ProjectsRecord _$result;
    try {
      _$result = _$v ??
          new _$ProjectsRecord._(
              projectName: projectName,
              projectType: projectType,
              status: status,
              clientCompany: clientCompany,
              clientContact: clientContact,
              projectSequenceNumber: projectSequenceNumber,
              currentInstalmentIndex: currentInstalmentIndex,
              statusCurrentInstalment: statusCurrentInstalment,
              projectDescription: projectDescription,
              tasks: _tasks?.build(),
              teamLeader: teamLeader,
              notes: notes,
              files: _files?.build(),
              feedback: feedback,
              ffRef: ffRef);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'tasks';
        _tasks?.build();

        _$failedField = 'files';
        _files?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ProjectsRecord', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
