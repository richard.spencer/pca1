// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instalments_record.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<InstalmentsRecord> _$instalmentsRecordSerializer =
    new _$InstalmentsRecordSerializer();

class _$InstalmentsRecordSerializer
    implements StructuredSerializer<InstalmentsRecord> {
  @override
  final Iterable<Type> types = const [InstalmentsRecord, _$InstalmentsRecord];
  @override
  final String wireName = 'InstalmentsRecord';

  @override
  Iterable<Object?> serialize(Serializers serializers, InstalmentsRecord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.order;
    if (value != null) {
      result
        ..add('order')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.amount;
    if (value != null) {
      result
        ..add('amount')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.dueDate;
    if (value != null) {
      result
        ..add('due_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.paidDate;
    if (value != null) {
      result
        ..add('paid_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DateTime)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ffRef;
    if (value != null) {
      result
        ..add('Document__Reference__Field')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                DocumentReference, const [const FullType.nullable(Object)])));
    }
    return result;
  }

  @override
  InstalmentsRecord deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new InstalmentsRecordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'order':
          result.order = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'amount':
          result.amount = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'due_date':
          result.dueDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'paid_date':
          result.paidDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'Document__Reference__Field':
          result.ffRef = serializers.deserialize(value,
              specifiedType: const FullType(DocumentReference, const [
                const FullType.nullable(Object)
              ])) as DocumentReference<Object?>?;
          break;
      }
    }

    return result.build();
  }
}

class _$InstalmentsRecord extends InstalmentsRecord {
  @override
  final int? order;
  @override
  final String? description;
  @override
  final double? amount;
  @override
  final DateTime? dueDate;
  @override
  final DateTime? paidDate;
  @override
  final String? status;
  @override
  final DocumentReference<Object?>? ffRef;

  factory _$InstalmentsRecord(
          [void Function(InstalmentsRecordBuilder)? updates]) =>
      (new InstalmentsRecordBuilder()..update(updates))._build();

  _$InstalmentsRecord._(
      {this.order,
      this.description,
      this.amount,
      this.dueDate,
      this.paidDate,
      this.status,
      this.ffRef})
      : super._();

  @override
  InstalmentsRecord rebuild(void Function(InstalmentsRecordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  InstalmentsRecordBuilder toBuilder() =>
      new InstalmentsRecordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is InstalmentsRecord &&
        order == other.order &&
        description == other.description &&
        amount == other.amount &&
        dueDate == other.dueDate &&
        paidDate == other.paidDate &&
        status == other.status &&
        ffRef == other.ffRef;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, order.hashCode), description.hashCode),
                        amount.hashCode),
                    dueDate.hashCode),
                paidDate.hashCode),
            status.hashCode),
        ffRef.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'InstalmentsRecord')
          ..add('order', order)
          ..add('description', description)
          ..add('amount', amount)
          ..add('dueDate', dueDate)
          ..add('paidDate', paidDate)
          ..add('status', status)
          ..add('ffRef', ffRef))
        .toString();
  }
}

class InstalmentsRecordBuilder
    implements Builder<InstalmentsRecord, InstalmentsRecordBuilder> {
  _$InstalmentsRecord? _$v;

  int? _order;
  int? get order => _$this._order;
  set order(int? order) => _$this._order = order;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  double? _amount;
  double? get amount => _$this._amount;
  set amount(double? amount) => _$this._amount = amount;

  DateTime? _dueDate;
  DateTime? get dueDate => _$this._dueDate;
  set dueDate(DateTime? dueDate) => _$this._dueDate = dueDate;

  DateTime? _paidDate;
  DateTime? get paidDate => _$this._paidDate;
  set paidDate(DateTime? paidDate) => _$this._paidDate = paidDate;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  DocumentReference<Object?>? _ffRef;
  DocumentReference<Object?>? get ffRef => _$this._ffRef;
  set ffRef(DocumentReference<Object?>? ffRef) => _$this._ffRef = ffRef;

  InstalmentsRecordBuilder() {
    InstalmentsRecord._initializeBuilder(this);
  }

  InstalmentsRecordBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _order = $v.order;
      _description = $v.description;
      _amount = $v.amount;
      _dueDate = $v.dueDate;
      _paidDate = $v.paidDate;
      _status = $v.status;
      _ffRef = $v.ffRef;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(InstalmentsRecord other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$InstalmentsRecord;
  }

  @override
  void update(void Function(InstalmentsRecordBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  InstalmentsRecord build() => _build();

  _$InstalmentsRecord _build() {
    final _$result = _$v ??
        new _$InstalmentsRecord._(
            order: order,
            description: description,
            amount: amount,
            dueDate: dueDate,
            paidDate: paidDate,
            status: status,
            ffRef: ffRef);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
