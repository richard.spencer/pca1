import 'dart:async';

import 'index.dart';
import 'serializers.dart';
import 'package:built_value/built_value.dart';

part 'tasks_record.g.dart';

abstract class TasksRecord implements Built<TasksRecord, TasksRecordBuilder> {
  static Serializer<TasksRecord> get serializer => _$tasksRecordSerializer;

  @BuiltValueField(wireName: 'task_name')
  String? get taskName;

  @BuiltValueField(wireName: 'team_members')
  BuiltList<DocumentReference>? get teamMembers;

  @BuiltValueField(wireName: 'start_date')
  DateTime? get startDate;

  @BuiltValueField(wireName: 'end_date')
  DateTime? get endDate;

  int? get prority;

  int? get order;

  @BuiltValueField(wireName: kDocumentReferenceField)
  DocumentReference? get ffRef;
  DocumentReference get reference => ffRef!;

  DocumentReference get parentReference => reference.parent.parent!;

  static void _initializeBuilder(TasksRecordBuilder builder) => builder
    ..taskName = ''
    ..teamMembers = ListBuilder()
    ..prority = 0
    ..order = 0;

  static Query<Map<String, dynamic>> collection([DocumentReference? parent]) =>
      parent != null
          ? parent.collection('Tasks')
          : FirebaseFirestore.instance.collectionGroup('Tasks');

  static DocumentReference createDoc(DocumentReference parent) =>
      parent.collection('Tasks').doc();

  static Stream<TasksRecord> getDocument(DocumentReference ref) => ref
      .snapshots()
      .map((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  static Future<TasksRecord> getDocumentOnce(DocumentReference ref) => ref
      .get()
      .then((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  TasksRecord._();
  factory TasksRecord([void Function(TasksRecordBuilder) updates]) =
      _$TasksRecord;

  static TasksRecord getDocumentFromData(
          Map<String, dynamic> data, DocumentReference reference) =>
      serializers.deserializeWith(serializer,
          {...mapFromFirestore(data), kDocumentReferenceField: reference})!;
}

Map<String, dynamic> createTasksRecordData({
  String? taskName,
  DateTime? startDate,
  DateTime? endDate,
  int? prority,
  int? order,
}) {
  final firestoreData = serializers.toFirestore(
    TasksRecord.serializer,
    TasksRecord(
      (t) => t
        ..taskName = taskName
        ..teamMembers = null
        ..startDate = startDate
        ..endDate = endDate
        ..prority = prority
        ..order = order,
    ),
  );

  return firestoreData;
}
