import 'dart:async';

import 'index.dart';
import 'serializers.dart';
import 'package:built_value/built_value.dart';

part 'projects_record.g.dart';

abstract class ProjectsRecord
    implements Built<ProjectsRecord, ProjectsRecordBuilder> {
  static Serializer<ProjectsRecord> get serializer =>
      _$projectsRecordSerializer;

  @BuiltValueField(wireName: 'project_name')
  String? get projectName;

  @BuiltValueField(wireName: 'project_type')
  String? get projectType;

  String? get status;

  @BuiltValueField(wireName: 'client_company')
  DocumentReference? get clientCompany;

  @BuiltValueField(wireName: 'client_contact')
  DocumentReference? get clientContact;

  @BuiltValueField(wireName: 'project_sequence_number')
  int? get projectSequenceNumber;

  @BuiltValueField(wireName: 'current_instalment_index')
  int? get currentInstalmentIndex;

  @BuiltValueField(wireName: 'status_current_instalment')
  String? get statusCurrentInstalment;

  @BuiltValueField(wireName: 'project_description')
  String? get projectDescription;

  BuiltList<DocumentReference>? get tasks;

  @BuiltValueField(wireName: 'team_leader')
  DocumentReference? get teamLeader;

  String? get notes;

  BuiltList<String>? get files;

  String? get feedback;

  @BuiltValueField(wireName: kDocumentReferenceField)
  DocumentReference? get ffRef;
  DocumentReference get reference => ffRef!;

  static void _initializeBuilder(ProjectsRecordBuilder builder) => builder
    ..projectName = ''
    ..projectType = ''
    ..status = ''
    ..projectSequenceNumber = 0
    ..currentInstalmentIndex = 0
    ..statusCurrentInstalment = ''
    ..projectDescription = ''
    ..tasks = ListBuilder()
    ..notes = ''
    ..files = ListBuilder()
    ..feedback = '';

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('Projects');

  static Stream<ProjectsRecord> getDocument(DocumentReference ref) => ref
      .snapshots()
      .map((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  static Future<ProjectsRecord> getDocumentOnce(DocumentReference ref) => ref
      .get()
      .then((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  ProjectsRecord._();
  factory ProjectsRecord([void Function(ProjectsRecordBuilder) updates]) =
      _$ProjectsRecord;

  static ProjectsRecord getDocumentFromData(
          Map<String, dynamic> data, DocumentReference reference) =>
      serializers.deserializeWith(serializer,
          {...mapFromFirestore(data), kDocumentReferenceField: reference})!;
}

Map<String, dynamic> createProjectsRecordData({
  String? projectName,
  String? projectType,
  String? status,
  DocumentReference? clientCompany,
  DocumentReference? clientContact,
  int? projectSequenceNumber,
  int? currentInstalmentIndex,
  String? statusCurrentInstalment,
  String? projectDescription,
  DocumentReference? teamLeader,
  String? notes,
  String? feedback,
}) {
  final firestoreData = serializers.toFirestore(
    ProjectsRecord.serializer,
    ProjectsRecord(
      (p) => p
        ..projectName = projectName
        ..projectType = projectType
        ..status = status
        ..clientCompany = clientCompany
        ..clientContact = clientContact
        ..projectSequenceNumber = projectSequenceNumber
        ..currentInstalmentIndex = currentInstalmentIndex
        ..statusCurrentInstalment = statusCurrentInstalment
        ..projectDescription = projectDescription
        ..tasks = null
        ..teamLeader = teamLeader
        ..notes = notes
        ..files = null
        ..feedback = feedback,
    ),
  );

  return firestoreData;
}
