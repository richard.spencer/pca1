import 'dart:async';

import 'index.dart';
import 'serializers.dart';
import 'package:built_value/built_value.dart';

part 'instalments_record.g.dart';

abstract class InstalmentsRecord
    implements Built<InstalmentsRecord, InstalmentsRecordBuilder> {
  static Serializer<InstalmentsRecord> get serializer =>
      _$instalmentsRecordSerializer;

  int? get order;

  String? get description;

  double? get amount;

  @BuiltValueField(wireName: 'due_date')
  DateTime? get dueDate;

  @BuiltValueField(wireName: 'paid_date')
  DateTime? get paidDate;

  String? get status;

  @BuiltValueField(wireName: kDocumentReferenceField)
  DocumentReference? get ffRef;
  DocumentReference get reference => ffRef!;

  DocumentReference get parentReference => reference.parent.parent!;

  static void _initializeBuilder(InstalmentsRecordBuilder builder) => builder
    ..order = 0
    ..description = ''
    ..amount = 0.0
    ..status = '';

  static Query<Map<String, dynamic>> collection([DocumentReference? parent]) =>
      parent != null
          ? parent.collection('Instalments')
          : FirebaseFirestore.instance.collectionGroup('Instalments');

  static DocumentReference createDoc(DocumentReference parent) =>
      parent.collection('Instalments').doc();

  static Stream<InstalmentsRecord> getDocument(DocumentReference ref) => ref
      .snapshots()
      .map((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  static Future<InstalmentsRecord> getDocumentOnce(DocumentReference ref) => ref
      .get()
      .then((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  InstalmentsRecord._();
  factory InstalmentsRecord([void Function(InstalmentsRecordBuilder) updates]) =
      _$InstalmentsRecord;

  static InstalmentsRecord getDocumentFromData(
          Map<String, dynamic> data, DocumentReference reference) =>
      serializers.deserializeWith(serializer,
          {...mapFromFirestore(data), kDocumentReferenceField: reference})!;
}

Map<String, dynamic> createInstalmentsRecordData({
  int? order,
  String? description,
  double? amount,
  DateTime? dueDate,
  DateTime? paidDate,
  String? status,
}) {
  final firestoreData = serializers.toFirestore(
    InstalmentsRecord.serializer,
    InstalmentsRecord(
      (i) => i
        ..order = order
        ..description = description
        ..amount = amount
        ..dueDate = dueDate
        ..paidDate = paidDate
        ..status = status,
    ),
  );

  return firestoreData;
}
