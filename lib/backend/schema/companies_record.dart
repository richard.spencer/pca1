import 'dart:async';

import 'index.dart';
import 'serializers.dart';
import 'package:built_value/built_value.dart';

part 'companies_record.g.dart';

abstract class CompaniesRecord
    implements Built<CompaniesRecord, CompaniesRecordBuilder> {
  static Serializer<CompaniesRecord> get serializer =>
      _$companiesRecordSerializer;

  @BuiltValueField(wireName: 'company_name')
  String? get companyName;

  String? get email;

  @BuiltValueField(wireName: 'phone_number')
  String? get phoneNumber;

  String? get address;

  @BuiltValueField(wireName: 'postal_code')
  String? get postalCode;

  String? get website;

  String? get uid;

  DocumentReference? get contact;

  @BuiltValueField(wireName: kDocumentReferenceField)
  DocumentReference? get ffRef;
  DocumentReference get reference => ffRef!;

  static void _initializeBuilder(CompaniesRecordBuilder builder) => builder
    ..companyName = ''
    ..email = ''
    ..phoneNumber = ''
    ..address = ''
    ..postalCode = ''
    ..website = ''
    ..uid = '';

  static CollectionReference get collection =>
      FirebaseFirestore.instance.collection('Companies');

  static Stream<CompaniesRecord> getDocument(DocumentReference ref) => ref
      .snapshots()
      .map((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  static Future<CompaniesRecord> getDocumentOnce(DocumentReference ref) => ref
      .get()
      .then((s) => serializers.deserializeWith(serializer, serializedData(s))!);

  CompaniesRecord._();
  factory CompaniesRecord([void Function(CompaniesRecordBuilder) updates]) =
      _$CompaniesRecord;

  static CompaniesRecord getDocumentFromData(
          Map<String, dynamic> data, DocumentReference reference) =>
      serializers.deserializeWith(serializer,
          {...mapFromFirestore(data), kDocumentReferenceField: reference})!;
}

Map<String, dynamic> createCompaniesRecordData({
  String? companyName,
  String? email,
  String? phoneNumber,
  String? address,
  String? postalCode,
  String? website,
  String? uid,
  DocumentReference? contact,
}) {
  final firestoreData = serializers.toFirestore(
    CompaniesRecord.serializer,
    CompaniesRecord(
      (c) => c
        ..companyName = companyName
        ..email = email
        ..phoneNumber = phoneNumber
        ..address = address
        ..postalCode = postalCode
        ..website = website
        ..uid = uid
        ..contact = contact,
    ),
  );

  return firestoreData;
}
