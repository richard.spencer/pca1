import '../auth/auth_util.dart';
import '../backend/backend.dart';
import '../components/instalment_card_widget.dart';
import '../flutter_flow/flutter_flow_icon_button.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../custom_code/actions/index.dart' as actions;
import '../flutter_flow/custom_functions.dart' as functions;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_fonts/google_fonts.dart';

class MyInstalmentsWidget extends StatefulWidget {
  const MyInstalmentsWidget({
    Key? key,
    this.project,
    this.projectName,
  }) : super(key: key);

  final DocumentReference? project;
  final String? projectName;

  @override
  _MyInstalmentsWidgetState createState() => _MyInstalmentsWidgetState();
}

class _MyInstalmentsWidgetState extends State<MyInstalmentsWidget> {
  InstalmentsRecord? createdInstallments;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    // On page load action.
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      await actions.zeroHighestOrder();
    });

    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: FlutterFlowTheme.of(context).secondaryBackground,
        automaticallyImplyLeading: false,
        leading: FlutterFlowIconButton(
          borderColor: Colors.transparent,
          borderRadius: 30,
          borderWidth: 1,
          buttonSize: 60,
          icon: Icon(
            Icons.chevron_left_rounded,
            color: FlutterFlowTheme.of(context).primaryText,
            size: 30,
          ),
          onPressed: () async {
            context.pop();
          },
        ),
        title: Text(
          'Instalments, Project: ${widget.projectName}',
          style: FlutterFlowTheme.of(context).title3,
        ),
        actions: [],
        centerTitle: false,
        elevation: 0,
      ),
      backgroundColor: FlutterFlowTheme.of(context).secondaryBackground,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  FlutterFlowIconButton(
                    borderColor: Colors.transparent,
                    borderRadius: 30,
                    borderWidth: 1,
                    buttonSize: 60,
                    icon: Icon(
                      Icons.add_circle_outline_sharp,
                      color: FlutterFlowTheme.of(context).primaryText,
                      size: 30,
                    ),
                    onPressed: () async {
                      final instalmentsCreateData = createInstalmentsRecordData(
                        order:
                            functions.incrementInt(FFAppState().highestOrder),
                        description: 'Uknown',
                        amount: 0.0,
                        dueDate: getCurrentTimestamp,
                        paidDate: getCurrentTimestamp,
                        status: '0',
                      );
                      var instalmentsRecordReference =
                          InstalmentsRecord.createDoc(widget.project!);
                      await instalmentsRecordReference
                          .set(instalmentsCreateData);
                      createdInstallments =
                          InstalmentsRecord.getDocumentFromData(
                              instalmentsCreateData,
                              instalmentsRecordReference);

                      setState(() {});
                    },
                  ),
                  Text(
                    'Add instalment',
                    style: FlutterFlowTheme.of(context).bodyText1,
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 0),
                  child: StreamBuilder<List<InstalmentsRecord>>(
                    stream: queryInstalmentsRecord(
                      parent: widget.project,
                      queryBuilder: (instalmentsRecord) =>
                          instalmentsRecord.orderBy('order'),
                      limit: 20,
                    ),
                    builder: (context, snapshot) {
                      // Customize what your widget looks like when it's loading.
                      if (!snapshot.hasData) {
                        return Center(
                          child: SizedBox(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator(
                              color: FlutterFlowTheme.of(context).primaryColor,
                            ),
                          ),
                        );
                      }
                      List<InstalmentsRecord> listViewInstalmentsRecordList =
                          snapshot.data!;
                      return ListView.builder(
                        padding: EdgeInsets.zero,
                        scrollDirection: Axis.vertical,
                        itemCount: listViewInstalmentsRecordList.length,
                        itemBuilder: (context, listViewIndex) {
                          final listViewInstalmentsRecord =
                              listViewInstalmentsRecordList[listViewIndex];
                          return Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  InstalmentCardWidget(
                                    order: listViewInstalmentsRecord.order,
                                    description:
                                        listViewInstalmentsRecord.description,
                                    amount: listViewInstalmentsRecord.amount,
                                    instalment:
                                        listViewInstalmentsRecord.reference,
                                    status: listViewInstalmentsRecord.status,
                                    dueDate: listViewInstalmentsRecord.dueDate,
                                    paidDate:
                                        listViewInstalmentsRecord.paidDate,
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
              Text(
                'Hello World',
                style: FlutterFlowTheme.of(context).bodyText1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
