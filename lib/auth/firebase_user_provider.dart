import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

class Pca1FirebaseUser {
  Pca1FirebaseUser(this.user);
  User? user;
  bool get loggedIn => user != null;
}

Pca1FirebaseUser? currentUser;
bool get loggedIn => currentUser?.loggedIn ?? false;
Stream<Pca1FirebaseUser> pca1FirebaseUserStream() => FirebaseAuth.instance
    .authStateChanges()
    .debounce((user) => user == null && !loggedIn
        ? TimerStream(true, const Duration(seconds: 1))
        : Stream.value(user))
    .map<Pca1FirebaseUser>((user) => currentUser = Pca1FirebaseUser(user));
