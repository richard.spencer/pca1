import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'lat_lng.dart';
import 'place.dart';
import '../backend/backend.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../../auth/auth_util.dart';

int stringToInt(String? ss) {
  int? i = int.tryParse(ss!);
  if (i == null) return 0;
  return i;
}

double stringToDouble(String? ss) {
  double? x = double.tryParse(ss!);
  if (x == null) return 0.0;
  return x;
}

String intToString(int? ii) {
  return ii.toString();
}

int incrementInt(int? ii) {
  return ii! + 1;
}
